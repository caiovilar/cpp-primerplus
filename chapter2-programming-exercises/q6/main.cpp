#include <iostream>

int main(int argc, char *argv[])
{
	double l_years;
	std::cout << "Enter the number of light years: ";
	std::cin >> l_years;
	std::cout << l_years << " light years = " << l_years*63240.0 << " astronomical units." << std::endl;
	return 0;
}

