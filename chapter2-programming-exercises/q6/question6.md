
# Question 6:
	Write a program that has a main() call a user-defined function that takes a distance in light years as an argument and then returns the distance in astronomical units (au). The program should request the light year value as input from the user and display the result, as shown in the following code:

	```C
	Enter the number of light yaers: 4.2
	4.2 light years = 265608 astronomical units.
	```
