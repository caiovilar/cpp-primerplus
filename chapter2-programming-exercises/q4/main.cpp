#include <iostream>

int main(int argc, char *argv[])
{
	int years;
	std::cout << "What's your current age in years?" << std::endl;
	std::cin >> years;
	std::cout << "You have lived " << years*12 << " months until now!" << std::endl;
	return 0;
}
