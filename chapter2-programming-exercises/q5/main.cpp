#include <iostream>

int main(int argc, char *argv[])
{
	double Celsius;
	std::cout << "Please enter a Celsius value: ";
	std::cin >> Celsius;
	std::cout << Celsius << " degrees Celsius is " << ((Celsius*1.8)+32.0) << " degrees Fahrenheit." << std::endl;
	return 0;
}
