
# Question 5:
	Write a program that has min() call a user-defined function that takes a celsius temperature value as an argument and then returns the equivalent Fahrenheit value. The program should request the Celsius value as input from the user and display the result, as shown in the following code:

```C
Please enter a Celsius value: 20
20 degrees Celsius is 68 degrees Fahrenheit.
```
