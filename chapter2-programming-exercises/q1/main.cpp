#include <iostream>
#include <string>


int main(int argc, char *argv[])
{
	std::string name, surname,streetaddr;
	int streetnumbr;
	std::cout << "Please state your name: " << std::endl;
	std::cin >> name;
	std::cout << "Please state your surname: " << std::endl;
	std::cin >> surname;
	std::cout << "Please insert the name of your street: " << std::endl;
	std::cin >> streetaddr;
	std::cout << "Please insert the number of your residence in the given street: " << std::endl;  
	std::cin >> streetnumbr;
	std::cout << "Hello, " << name << " " << surname << std::endl;
	std::cout << "Who lives at " << streetnumbr << ", " << streetaddr << "!" << std::endl; 
	return 0;
}
