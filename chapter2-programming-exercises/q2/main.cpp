#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
	double furlongs;
	std::cout << "How many furlongs to yards: ";
	std::cin >> furlongs;
	std::cout << "This is equal to " << furlongs*220 << " yards." << std::endl;
	return 0;
}

