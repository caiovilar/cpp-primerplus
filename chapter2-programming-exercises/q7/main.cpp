#include <iostream>
 
int main(int argc, char *argv[])
{
	int hours, minutes;
	std::cout << "Enter the number of hours: ";
	std::cin >> hours;
	std::cout << "Enter the value of minutes: ";
	std::cin >> minutes;
	std::cout << "Time: " << hours << ":" << minutes << std::endl;
	return 0;
}
