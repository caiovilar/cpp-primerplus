#include <iostream>

const double km_2_miles = 0.6213;
const double gallon_2_liters = 3.875;

int main (int argc, char *argv[])
{
	double liter_km100 = 0.0;
	std::cout << "Enter the automobile consumption in L/ 100 km: ";
	std::cin >> liter_km100;
	std::cout << "The automobile consumption in miles/galon is: "
						<< ((100/liter_km100)*(gallon_2_liters/(1/km_2_miles)))<< std::endl;
	return 0;
}
