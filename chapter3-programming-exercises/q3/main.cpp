#include <iostream>
#include <iomanip>

int main(int argc, char *argv[])
{
	const double min_2_deg = (1.0/60.0);
	const double sec_2_deg = (1.0/3600.0);
	double deg, minutes,sec,deg_conv;
	std::cout << "Enter a latitude in degrees, minutes, and seconds:" << std::endl;
	std::cout << "First, enter the degrees: ";
	std::cin >> deg;
	std::cout << "Next, enter the minutes of arc: ";
	std::cin >> minutes;
	std::cout << "Finally, enter the seconds of arc: ";
	std::cin >> sec;
	deg_conv = (deg+(minutes*min_2_deg)+(sec*sec_2_deg));
	std::cout << deg << " degrees, " << minutes << " minutes, " << sec << " seconds = " << deg_conv << " degrees" << std::endl;
	return 0;
}
