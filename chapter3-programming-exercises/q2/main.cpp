#include <iostream>
#include <cmath>
const int ft_2_inches = 12;
const double inch_2_meters = 0.0254;
const float pounds_2_kilog = 2.2;

int main(int argc, char *argv[])
{
	float ft_n_inches,pounds,height_m,kilo,BMI;


	std::cout << "Please insert your height in feets and inches: ";
	std::cin >> ft_n_inches;
	std::cout << "Please insert your weight in pounds: ";
	std::cin >> pounds;
	height_m = (ft_n_inches*ft_2_inches)*inch_2_meters;
	kilo = (pounds/pounds_2_kilog);
	BMI = (kilo/pow(height_m,2));
	std::cout << "Your current Body Mass Index (BMI) is: " << BMI << std::endl;
	if(BMI < 18.5)
	{
		std::cout << "You are underweight!" << std::endl;
	}
	else if((BMI > 18.5) && (BMI < 24.9))
	{
		std::cout << "You are in your normal recommended weight." << std::endl;
	}
	else if((BMI > 25.0) && (BMI < 29.9))
	{
		std::cout << "Your are overweight!" << std::endl;
	}
	else if(BMI >= 30.0)
	{
		std::cout << "You are obese, start exercising!!" << std::endl;
	}

	return 0;
}
