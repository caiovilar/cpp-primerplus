#include <iostream>

int main(int argc, char *argv[])
{
	double miles = 0.0;
	double gallon_gas = 0.0;

	std::cout << "Enter how many miles you have driven: ";
	std::cin >> miles;
	std::cout << "Enter how many gallons of gasoline your car have consumed: ";
	std::cin >> gallon_gas;
	std::cout << "Miles per Gallon of your car: " << miles/gallon_gas << std::endl;
	return 0;
}
