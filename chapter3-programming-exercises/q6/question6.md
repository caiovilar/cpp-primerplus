# Question 6:
	Write a program that asks how many miles you have driven and how many gallons of gasoline you have
	used and then report the miles per gallon your car has gotten.
	Or, if you prefer the program can request distance in kilometers and petrol in liters
	and the report the result European style, in liters per kilometers.
