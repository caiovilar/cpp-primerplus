#include <iostream>
#include <iomanip>
#include <string>

const int hours_2_days = 24;
const int minutes_2_hours = 60;
const int seconds_2_minutes = 60;

int main(int argc, char *argv[])
{
	long long int seconds = 0;
	int days = 0;
	int hours = 0;
	int minutes = 0;
	int seconds_res = 0;
	std::cout << "Enter the number of seconds: ";
	std::cin >> seconds;
	seconds_res = seconds;
	days = (((seconds_res/seconds_2_minutes)/(minutes_2_hours))/hours_2_days);
	seconds_res -= (days*hours_2_days*minutes_2_hours*seconds_2_minutes);
	hours = ((seconds_res/seconds_2_minutes)/minutes_2_hours);
	seconds_res -= (hours*minutes_2_hours*seconds_2_minutes);
	minutes = (seconds_res/seconds_2_minutes);
	seconds_res -= (minutes*seconds_2_minutes);
	std::cout << seconds << " seconds = " 
						<< days << " days, " 
						<< hours << " hours, " 
						<< minutes << " minutes, " 
						<< seconds_res << " seconds" << std::endl;
	return 0;
}
