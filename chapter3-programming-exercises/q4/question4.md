
# Question 4:
	Write a program that asks the user to enter the number of seconds as an integer value (use type long, or if available, long long)
	and that then displays the equivalent time in days, hours, minutes and seconds. 
	Use symbolic constants to represent the number of hours in the day, the number of minutes in an hour, and the number of seconds in a minute.
	The output shoud look like this:
	
```cpp
Enter the number of seconds: 31600000
31600000 seconds  = 365 days, 17 hours, 46 minutes, 40 seconds
```
