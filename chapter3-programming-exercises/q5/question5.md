# Question 5:
	Write a program thatrequests the suer to enter the current world population and the current population of the U.S (or of some other nation of your choice).
	Store the information in variables of type long long.
	Have the program display the percent that the U.S. (or other nation of your choice) population
	is of the world's population.
	The ouput should look like this:

```cpp
Enter the world's population: 6898758899
Enter the population of the U.S.: 310783781
The population of the U.S. is 4.50492% of the world population.
```
