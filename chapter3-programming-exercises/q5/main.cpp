#include <iostream>

int main(int argc, char *argv[])
{
	long long int countrys_pop = 0;
	long long int world_population = 0;
	std::cout << "Enter the world's population: ";
	std::cin >> world_population;
	std::cout << "Enter the population of the U.S: ";
	std::cin >> countrys_pop;
	std::cout << "The population of the U.S is " << ((double)(countrys_pop)/((double)world_population))*100.0 << "% of the world population." << std::endl;
	return 0;
}
