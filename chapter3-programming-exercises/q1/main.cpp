#include <iostream>

int main(int argc, char *argv[])
{
	int inches;
	const double in2ft = 0.0833333;
	std::cout << "Please enter your height in inches: ";
	std::cin >> inches;
	std::cout << "Your height in feet and inches is: " << inches*in2ft << std::endl;
	return 0;
}
